package com.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pizza365.model.*;
import com.pizza365.repository.*;

@RestController
public class CRegionController {
	@Autowired
	private RegionRepository regionRepository;
	@Autowired
	private CountryRepository countryRepository;

	@CrossOrigin
	@GetMapping("/region/all")
	public List<CRegion> getAllRegion() {
		return regionRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/region/countryid/{id}")
	public List<CRegion> getRegionByCountryId(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get().getRegions();
		else
			return null;
	}

	@CrossOrigin
	@GetMapping("/region/details/{id}")
	public CRegion getRegionById(@PathVariable Long id) {
		if (regionRepository.findById(id).isPresent())
			return regionRepository.findById(id).get();
		else
			return null;
	}
	
	@CrossOrigin
	@PostMapping("/region/create/{id}")
	public ResponseEntity<Object> createRegion(@PathVariable("id") Long id ,@RequestBody CRegion cRegion) {
		Optional<CCountry> countryData = countryRepository.findById(id);
		if(countryData.isPresent())
		try {
			CRegion newRole = new CRegion();
			newRole.setRegionName(cRegion.getRegionName());
			newRole.setRegionCode(cRegion.getRegionCode());
			newRole.setCountry(countryData.get());
			CRegion savedRole = regionRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@CrossOrigin
	@PutMapping("/region/update/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		Optional<CRegion> regionData = regionRepository.findById(id);
		if (regionData.isPresent()) {
			CRegion newRegion = regionData.get();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());
			CRegion savedRegion = regionRepository.save(newRegion);
			return new ResponseEntity<>(savedRegion, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin
	@DeleteMapping("/region/delete/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			regionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
