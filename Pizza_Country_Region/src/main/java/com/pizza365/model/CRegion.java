package com.pizza365.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "region")
public class CRegion {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true, name = "region_code")
	private String regionCode;

	@Column(name = "region_name")
	private String regionName;
	@ManyToOne
	@JsonIgnore
	private CCountry country;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	@JsonIgnore
	public CCountry getCountry() {
		return country;
	}

	public void setCountry(CCountry cCountry) {
		this.country = cCountry;
	}
	
}
